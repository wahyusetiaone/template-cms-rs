# Template HTML CMS RS

Response Template HTML5 with CSS3

## Getting Started

Get you a copy of the project up and running on your local machine for development and testing purposes.

## Running the tests

Clone Or Download then play fun

## Built With

* [HTML5] - Language Progamming Used
* [CSS3] - Language Style Used

## Authors

* **Wahyu Setiawan** - *Initial work* - [Wahyu Setiawan](https://github.com/wahyusetiaone)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

